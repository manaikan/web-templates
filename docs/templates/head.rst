.. _Head:

------
<Head>
------

.. todo:: Write

    This section needs to be written :

     * CSS Blocks
     * Meta Blocks
     * JS Blocks

Resets and Shivs
================

Due to the different Javascript and HTML implementations in different web browsers there has been a need to provide HTML "shivs", which extend the base set of tags supported by a browser, and CSS "resets" to normalize the default spacing, padding and margins of elements within a page.
Presently |Project| do not apply any particular "shiv" or "reset" nor does it provision any block(s) for this.
Should such blocks be required please open an issue.

.. note ::

    If there were a standard "shiv" or "reset" this package could certainly consider incorporating it but there seems to be no clear consensus on the matter.

.. Authors requiring this may make the following substitutions.
.. ::
..
..         <!--[if lt IE 9]><script src="js/html5shiv.js"></script><![endif]-->

