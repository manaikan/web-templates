----------
Extentions
----------

.. todo:: Review

    This section is dated and needs to be reviewed and rewritten.

3rd parties providing additional data, fitting within the scope of this package, are welcome to do so provided they do not overwrite the provided templates. 
Authors seeking to override these templates may do so by raising an issue accordingly upon the gitlab interface for the repository.
Ideally 3rd parties would name their package with reference to this one, :mod:`template-PACKAGE`, but this is not always feasible especially when the templates are provided as part of a larger unrelaetd package.
In this case one asks that authors make reference to this package within their documentation, include it as a dependency within their :file:`setup.py` script.
Ideally such templates would then reside under a subpackage/folder as in templates.PACKAGE.*.
Major frameworks may ontribute a template within the root namespace but should inform the maintainers of :mod:`templates` by raising an issue.
Should any conflicts arise as a result these will be resolved by discussion betwen the parties involved or if no discussion is possible then the first party reserving a name will win.
