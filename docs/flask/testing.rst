-------
Testing
-------

PyTest-Flask
============

This sets up the fixtures commonly required by ones PyTest tests.

.. code-block:: python
    :caption: :file:`test/conftest.py`

    import pytest
    from PACKAGE import create_app

    @pytest.fixture
    def app() :
        return create_app

