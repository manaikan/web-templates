------
Sphinx
------

The :ref:`Sphinx-Django <sphinx/django:Django>` section describes how to convert ones Sphinx sources into an intermediate target that is embeddable within Django.
This section describes how to embed this intermediate source into ones pages served by Django.

.. note ;;

    This setup is only meant to cater for sites with a single set of documentation.
    It becomes rather cumbersome when one tries to set this up for multiple sets of documentation.
    If the latter is ones goal one should review the ReadTheDocs source service or consider :mod:`django-sphinxdoc`

Configuration
=============

Given a standard Django project structure :

+-----------------------------+-----------------------------+
| Website                     | Web Application             |
+-----------------------------+-----------------------------+
|::                           |::                           |
|                             |                             |
|  PROJECT                    |  PROJECT                    |
|   +- WEBAPP                 |   +- WEBAPP                 |
|   |   +- settings.py        |       +- templates          |
|   |   +- views.py           |           +- DOCUMENTATION  |
|   |   +- urls.py            |       +- settings.py        |
|   |   +- ...                |       +- views.py           |
|   +- templates              |       +- urls.py            |
|       +- DOCUMENTATION      |       +- ...                |
|                             |                             |
+-----------------------------+-----------------------------+

Where :file:`WEBAPP` is either the website or a web application created by ``django-admin``.
Perform the following operations from within the :file:`PROJECT` root.

Builds ones documentation, setting ``TARGET`` to :file:`templates/DOCUMENTATION` for a website, :

.. code-block:: console

    sphinx-build -D html_theme=sphinx_django [OPTIONS] SOURCE templates/DOCUMENTATION [FILES...]

or :file:`WEBAPP/templates/DOCUMENTATION` for a web application

.. code-block:: console

    sphinx-build -D html_theme=sphinx_django [OPTIONS] SOURCE WEBAPP/templates/DOCUMENTATION [FILES...]

Include the :file:`templates` folder as a source for templates for the website :

.. code-block::
    :caption: :file:`WEBAPP/settings.py`

    TEMPLATES = [...
            'DIRS': [BASE_DIR + "/templates/"],
     ...]

or include the web application within ones site configuration

.. code-block::
    :caption: :file:`WEBAPP/settings.py`

    INSTALLED_APPS = [...
            "WEBAPP",
     ...]


Create a :ref:`base template <django:Base Template(s)>` named :file:`base.sphinx.html` (:download:`Click to download <sphinx.html>`).
Provide a view function

.. code-block::
    :caption: :file:`WEBAPP/views.py`

    import os
    from django.shortcuts import render
    from django.http import HttpResponse, HttpResponseNotFound

    def document(request, template_name = "index.html"):
        return HttpResponse(render(request, template_name=os.sep.join(['DOCUMENTATION', template_name])))

and register it under the url path :file:`/documents/`.

.. code-block::
    :caption: :file:`WEBAPP/urls.py`

    from django.urls import path
    from . import views
    ...

    urlpatterns = [...
        path('<path:template_name>', views.document, name='WEBAPP'),
    ...]

If your setup is for a web application be sure to link across to it from the website.

.. code-block::
    :caption: :file:`WEBAPP/urls.py`

    from django.urls import path
    from django.urls import include
    from . import views
    ...

    urlpatterns = [ ...
        path('DOCUMENTATION/',   include("WEBAPP.urls")),
    ...]

Multiple sets of documentaion can be hosted provided one replaces WEBAPP and DOCUMENATION uniquely for each such application.