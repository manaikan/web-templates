-----
Menus
-----

There are a number of Django applications providing menu support.
Currently only Django: Simple Menus is supported by web-templates.

Simple Menus
============

The :file:`templates/simple_menu.html`, under :file:`web_templates/django`, provides a "classical" HTML <ul/>/<li/> based menu.

The :file:`static/simple_menu.css`, under :file:`web_templates/django`, provide a very simple styling for this structure.
This highlights the currently active route and it's parents, collapsing all other branches.

To incorporate this into ones own project substitute the following into their :file:`templates/base.html` template ::

    {%  extends "skeleton.html" %}

    {% load menu %}
    {% load static %}

    {% block css_site %}
    <link rel="stylesheet" href="{% static 'simple_menu.css' %}">
    {% endblock css_site %}

    {% block nav_body %}
    {% generate_menu %}
    {% block nav_menu %}
    {% with menu=menus.main %}{% include "simple_menu.html" %}{% endwith %}
    {% endblock nav_menu %}
    {% endblock nav_body %}

.. note::

    The only concrete implementation of this is done within my Django:Fete project.
    Please review this project if you need to support this.

.. todo::

    Provide two styles :file:`simple_menus/horizontal.css` and :file:`simple_menus/verticul.css` for horizontal and/or vertical menus respectively.
    These style should both be applicable to the menus generated from :file:`simple_menus/navigation.html`.
    These files are to be adapted from `simple_menu.html` and `simple_menu.css` accordingly.

    Consider also that a better structure, semantically speaking, might be generated through `<div/>` and/or "description" elements.

.. todo::

    Once the simpler case for unordered lists is handled adapt this for other frontend frameworks e.g. bootstrap, tailwind etc.

