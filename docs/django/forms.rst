-----
Forms
-----

.. note::

    Internally django uses :file:`django/templates/forms.html` to generate its forms and this might be adapted for the purposes proposed below.

`Mozilla: Form Control Compatability <https://developer.mozilla.org/en-US/docs/Learn/Forms/Property_compatibility_table_for_form_controls>`_, `Mozilla: Form Controls (HTML 5) <https://developer.mozilla.org/en-US/docs/Learn/Forms/HTML5_input_types>`_  and `Mozilla: Client Side validation <https://developer.mozilla.org/en-US/docs/Learn/Forms/Form_validation>`_ discusses the various form controls.
`A list apart: Sensible forms <https://alistapart.com/article/sensibleforms/>`_ discusses some best practices.
`Mozilla: Form Controls <https://developer.mozilla.org/en-US/docs/Learn/Forms/How_to_build_custom_form_controls>`_ discusses the development of custom form controls.

`Mozilla: Form Structures <https://developer.mozilla.org/en-US/docs/Learn/Forms/How_to_structure_a_web_form>`_ covers the layout of forms.
CSS Tricks advise that ``<label>`` appear after ``<input>`` within ones HTML since CSS struggles to select a ``<label>`` that precedes its corresponding ``<input>`` but can readily swap their arrangement in the final layout and presentation of the form.
It seems that placing the ``<input>`` within the ``<label>`` is often discouraged, presumably since the front end folk can't alter the form structure/layout in this case.

`Mozilla: Form Styling <https://developer.mozilla.org/en-US/docs/Learn/Forms/Styling_web_forms>`_ and `Mozilla: Advanced Form Styling <https://developer.mozilla.org/en-US/docs/Learn/Forms/Advanced_form_styling>`_ cover the styling of forms.
`W3 School: CSS Selectors <https://www.w3schools.com/css/css_pseudo_elements.asp>`_ reminds one of a number of CSS selectors that are applicable to forms.

Definition
==========

A simple Django forms might be defined as follows ::

    class FORM(forms.Form):
        # See the :focus, :enabled and :disabled CSS pseudo selectors
        error_css_class = 'ERROR'       #: Javascript class applied to erroneous elements e.g. .ERROR { text-color:red; } (See the :in-range, :out-of-range, :valid and :invalid CSS pseudo selectors)
        required_css_class = 'REQUIRED' #: Javascript class applied to required elements e.g. .REQUIRED { text-style:bold; } .REQUIRED::before { content: *; } (See the :optional and :required CSS pseudo-selectors)

A model specific form is similarly defined ::

    class ArticleForm(forms.ModelForm):
        class Meta:
            model = Article
            fields = ['pub_date', 'headline', 'content', 'reporter']

`Django: Form Fields <https://docs.djangoproject.com/en/3.2/topics/forms/modelforms/>`_ lists the available field types.
`Schinckell: Form Fieldsets <https://schinckel.net/2013/06/14/django-fieldsets/>`_ shows a method for supporting Fieldsets.

`Django: Formsets <https://docs.djangoproject.com/en/3.2/topics/forms/formsets/>`_ are usually used to edit the collections of objects in another model from the current one.

Layout
======

Django supports a number of form layouts, out of the box it supports :

 * tabular (the default), ::

    <form method="POST" action="{% url "OBJECT-create" %}">
        <table>
            <thead>
                <tr><th>Field</th><th>Value</th></tr>
            </thead>
            <tbody>
                <tr><td colpsan="2">{% csrf_token %}</td></tr>
                {{ form or form.as_table }}
            </tbody>
            <tfoot>
                <tr><th colpsan="2"><input type="submit" value="Submit"/></th></tr>
            </tfoot>
        </table>
    </form>

    <form method="POST" action="APPLICAITON/OBJECT/create/">
        <table>
            <thead>
                <tr><th>Field</th><th>Value</th></tr>
            </thead>
            <tbody>
                <tr><td colpsan="2"><input type="hidden" name="csrfmiddlewaretoken" value="CSRF_TOKEN"></td></tr>
                <tr><th><label for="id_FIELD">Field:</label></th><td><input type="text" name="name" maxlength="150" required id="id_FIELD"></td></tr>
                <tr><th><label for="id_FIELD">Field:</label></th><td><input type="url" name="url" maxlength="200" required id="id_FIELD"></td></tr>
            </tbody>
            <tfoot>
                <tr><th colpsan="2"><input type="submit" value="Submit"/></th></tr>
            </tfoot>
        </table>
    </form>

 * listed and ::

    <form method="POST" action="{% url "OBJECT-create" %}">
        <ul>
            <li>{% csrf_token %}</li>
            {{ form.as_ul }}
        </ul>
        <input type="submit" value="Submit"/>
    </form>

    <form method="POST" action="APPLICAITON/OBJECT/create/">
        <ul>
            <li><input type="hidden" name="csrfmiddlewaretoken" value="CSRF_TOKEN"></li>
            <li><label for="id_name">Name:</label> <input type="text" name="name" maxlength="150" required id="id_name"></li>
            <li><label for="id_url">Url:</label> <input type="url" name="url" maxlength="200" required id="id_url"></li>
            <li><input type="submit" value="Submit"/></li>
        </ul>
    </form>

  * paragraphical layout(s). ::

    <form method="POST" action="{% url "OBJECT-create" %}">
        {% csrf_token %}
        {{ form.as_p }}
        <input type="submit" value="Submit"/>
    </form>

    <form method="POST" action="APPLICAITON/OBJECT/create/">
        <p><input type="hidden" name="csrfmiddlewaretoken" value="CSRF_TOKEN"></p>
        <p><label for="id_FIELD">FIELD:</label> <input type="text" name="name" maxlength="200" required id="id_FIELD"></p>
        <p><label for="id_FIELD">FIELD:</label> <input type="url" name="url" maxlength="200" required id="id_FIELD"></p>
        <p><input type="submit" value="Submit"/></p>
    </form>

For those seeking a bit more control over the generation fo their forms ::

    <form>
    {{ form.non_field_errors }}
    {% for field in form.visible_fields %}
        {# field.is_hidden #}
        <fieldset class="field">
        {{ field }}
        {{ field.label_tag }}
        {% if field.help_text %}
        <p class="help">{{ field.help_text|safe }}</p>
        {% endif %}
        {{ field.errors }}
        </fieldset>
    {% endfor %}
    {% for hidden in form.hidden_fields %}
        {{ hidden }}
    {% endfor %}
    </form>


    <form>
        <fieldset class="field">
        <input type="text" name="name" maxlength="150" required id="id_name">
        <label for="id_name">Name:</label>
        </fieldset>
        <fieldset class="field">
        <input type="url" name="url" maxlength="200" required id="id_url">
        <label for="id_url">Url:</label>
        </fieldset>
    </form>

For those seeking a bit more control Django allows one to peel away a layer and structure a form more explicitly.

    <form>
    {{ form.non_field_errors }}
    {% for field in form.visible_fields %}
        {# field.is_hidden #}
        <fieldset class="field">
        {{ field }}
        {{ field.label_tag }}
        {% if field.help_text %}
        <p class="help">{{ field.help_text|safe }}</p>
        {% endif %}
        {{ field.errors }}
        </fieldset>
    {% endfor %}
    {% for hidden in form.hidden_fields %}
        {{ hidden }}
    {% endfor %}
    </form>


    <form>
        <fieldset class="field">
        <input type="text" name="name" maxlength="150" required id="id_name">
        <label for="id_name">Name:</label>
        </fieldset>
        <fieldset class="field">
        <input type="url" name="url" maxlength="200" required id="id_url">
        <label for="id_url">Url:</label>
        </fieldset>
    </form>
