----
REST
----

Authentication
==============

The setup of :class:`SesssionAuthentication` is spread across both the Django and Django REST framework documentation.
Django provides a CSRF token through a cookie by default.
One can move this into the `users' session <https://docs.djangoproject.com/en/4.0/ref/settings/#csrf-use-sessions>`_ through :attr:`CSRF_USE_SESSIONS` and by enabling :class:`SessionMiddleware`.

.. note::

    Consider setting :attr:`CSRF_COOKIE_HTTPONLY` and :attr:`SESSION_COOKIE_HTTPONLY` to prevents javascript from accessing the CSRF token.
    Supposedly there are some minor security benefits to this and oocasionlly it might be required by some audits.

To access a Cookie from Javascript the Django documentation suggests the `Js-Cookie <https://github.com/js-cookie/js-cookie/>`_ project.
The latest release of this project (Version 3) has been included under :file:`static/cookies`.
One should include this within their templates as follows ::

    <script type="module" src="/path/to/js.cookie.mjs"></script><!-- Newer Browsers -->
    <script nomodule defer src="/path/to/js.cookie.js"></script><!-- Older Browsers -->

OR via CDN ::

    <script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.1/dist/js.cookie.min.js"></script>

Which allows one to retrieve the CSRF token using the following ::

    const csrftoken = Cookies.get('csrftoken');

Where the cookie is not available to javascript through a cookie one must extract it from the page as follows :

    {% csrf_token %}
    <script>
    const csrftoken = document.querySelector('[name=csrfmiddlewaretoken]').value;
    </script>

Finally to use the CSRF token in an AJAX call use ::

    const request = new Request(
    /* URL */,
    {
        method: 'POST',
        headers: {'X-CSRFToken': csrftoken},
        mode: 'same-origin' // Do not send CSRF token to another domain.
    }
    );
    fetch(request).then(function(response) {
        // ...
    });

See `CSRF <https://docs.djangoproject.com/en/4.0/ref/csrf/#acquiring-csrf-token-from-html>`_ for more.
