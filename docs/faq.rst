Frequently asked questions (some with Answers)
==============================================

* The browser keeps trying to translate the page ?

  The skeleton template requires one to include the necessary context processors so that the appropriate internationalization information is available within the context.
  This information is used to set the language for the page and when this is set correctly the browser will cease to ask if the content should be translated.

* Is there a base template that extends/inherits the skeleton template that one may readily include in their project ?

  The package provides such a template for each framework/template engine.
  Please see the framework/template engine specific sections accordingly.

