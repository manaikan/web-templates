======
Django
======

.. toctree::
   :maxdepth: 2
   :caption:  Contents:

   Sphinx         <django/sphinx>
   Menus          <django/menus>
   Forms          <django/forms>


.. .. table:: Setup specific configurations
..    :align:  center
..    :widths: grid
..    :name: usage matrix
..
..    +------------------------+--------------------------------------------+------------------------------+
..    |                        | :term:`Smarty`                             | :term:`Jinga`                |
..    +------------------------+--------------------------------------------+------------------------------+
..    | :term:`Django`         | :ref:`configure:Django + Template Engine`  | N/A                          |
..    +------------------------+--------------------------------------------+------------------------------+
..    | :term:`Flask`          | N/A                                        | N/A                          |
..    +------------------------+--------------------------------------------+------------------------------+
..    | :term:`Tornado`        | N/A                                        | N/A                          |
..    +------------------------+--------------------------------------------+------------------------------+

-------------
Configuration
-------------

Django requires that one register |Project| before it will acknowledge the templates that it provides.
Within ones :file:`WEBSITE/settings.py` file add :attr:`web_templates.django` under :attr:`INSTALLED_APPS` as shown ::

  INSTALLED_APPS = [...
    'web_templates.django',
    ...]

This makes the :file:`skeleton.html` available for extention by the :ref:`base template <django:Base Template(s)>`, :file:`base.html`, of ones project.

----------------
Base Template(s)
----------------

Once |Project| is :ref:`configured <django:Configuration>` one should create a project wide :file:`base.html` file for their website or web application.
Create the :file:`WEBSITE/templates/base.html` and extend the :file:`skeleton.html` as necessary.

.. code-block::
    :caption: :file:`base.html`

    {% extend "skeleton.html" %}

.. This file should extend the django :file:`skeleton.html` file to include any project wide settings.

Ideally the base template of ones web application(s), :file:`APPLICATION/base.html`, should both require and extend this template, :file:`base.html`, to effect their respective views.

.. code-block::
    :caption: :file:`APPLICATION/base.html`

    {% extend "base.html" %}

----------------
Generic Template
----------------

Even without a standardized template for Django ones finds that some combinations of the Django packages have implemented fairly similar conventions.
Certainly it seems feasible to provide a generic template for ones project that will accommodate these conventions with the skeleton provided by |Project|.

.. These templates enforce the use of the staticfiles package(s) which ship with Django and the sitetree package(s) which do not.
.. Later improvements might enforce the use of Angular and React, JQuery is dated and to be avoided if possible.

The following describes such a generic *base* file (:download:`Click to download <django.html>`).
First it extends the **skeleton** provided by |Project|.

.. literalinclude:: django.html
    :lines: 1

The following template processors and tags may then be enabled as one requires.

Debug
    It is common, under development, to view various debugging information.
    Django provides the `debug <https://docs.djangoproject.com/en/2.2/ref/templates/builtins/#debug>`_ template tag for this very purpose.
    `Stefano <https://stackoverflow.com/a/4051941>`_ suggests making this output available in the source of a page but hiding it normally.

    .. literalinclude:: django.html
        :lines: 4

Static Files

    The *base* template makes the media and static files available globally within a projects' templates.
    It might be best to handle this on the application level though

    .. literalinclude:: django.html
        :lines: 2

Site Trees
    Commonly one also needs to include some form of navigational component; sitetrees provides a thorough yet highly configurable structure for this.

    .. literalinclude:: django.html
        :lines: 3
