--------------------------
Search Engine Optimization
--------------------------

`Google <https://developers.google.com/search>`_ provides documentation on structured data and how it can be leveraged to improve ones search rankings.

`Data Vocabulary <http://www.data-vocabulary.org/>`_ and `Schema <https://schema.org/>`_ provide the common vocabulary used by teh search engines to identify the important information upon ones page.
`Business Card <http://thenewcode.com/667/Make-A-Digital-Business-Card-For-All-Devices>`_ provides oens site with a business card of sorts.

Examples
========

The `Search Gallery <https://developers.google.com/search/docs/guides/search-gallery>`_ provides a series of examples on how to set this sort of thing up quite nicely.

.. todo::

    Review how to show a Software Application, Bread Crumb, FAQ and How T links.

Testing
=======

`Google <https://developers.google.com/search/tools>`_

`Structured Date: Testing<https://search.google.com/structured-data/testing-tool>`_, `Rich Results: Test<https://search.google.com/test/rich-results?utm_campaign=devsite&utm_medium=jsonld&utm_source=local-business>`_ and generate a `Rich Results: Report <https://support.google.com/webmasters/answer/7552505>`_