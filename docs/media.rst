-----
Media
-----

`SO: No Script <https://stackoverflow.com/a/50900679/958580>`_

.. code-block:: css

    @media (scripting: none) {
        /* styles for when JS is disabled */
    }