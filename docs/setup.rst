
-----
Setup
-----

Installation
============

Users will typically install the package directly from the :ref:`Python Package Index (PyPI)` while developers and contributers may prefer a :ref:`source <Repository>` based installation.

Python Package Index (PyPI)
---------------------------

|Project| is hosted upon :term:`PyPI` and is installed most conveniently through :command:`pip`, the command line installation utility ::

    pip install web-templates

Users looking to use the bleeding edge version of the package can install it directly from the repository ::

    pip install git+https://gitlab.com/manaikan-open-source/web-templates/

Repository
----------

The project may be cloned from the `repository <gitlab.com/manaikan-open-source/web-templates/>`_ repository using `git`, the command line version control management utility, ::

    git clone https://gitlab.com/manaikan-open-source/web-templates/ web-templates

and subsequently installed through `pip` ::

    pip install -e .

Uninstallation
==============

Similarly, the package is uninstalled using :command:`pip` ::

    pip uninstall web-templates

