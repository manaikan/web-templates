------------
Contribution
------------

.. todo:: Review

    This section is dated and needs to be reviewed and rewritten.


.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   contribution/structure
   contribution/HTML
   contribution/packages
   contribution/tools
   Symbolic Links <contribution/symlinks>

:ref:`contribution/packages:Name Spaced Packages` discusses how one might distribute common template/data through Python packages.
Such packages, and related 3rd party extentions, allow for unhindered contribution, provided all parties adhere to a few conventions.
:ref:`contribution/structure:package structure` outlines the conventions for the web-templates package so that no one overrides or breaks anothers contribution.

.. todolist::




