--------
Glossary
--------

.. glossary::

   :abbr:`CSS (Cascading Style Sheets)` : CSS
     These code embedded within a webpage that specifies how it ought to look

   Django
     A web framework written in Python

   :abbr:`DTD (Document Type Declaration)` : DTD
     A tag at the top of an HTML document declaring it as one or other variant of HTML
  
   :abbr:`DTE (Django Template Engine)` : DTE
     The engine that processes the |DTL|

   :abbr:`DTL (Django Template Language)` : DTL
     The Django Template Lanugage

   Flask
     A micro-web framework written in Python

   :abbr:`HTML (Hyper Text Markup Language)` : HTML
     This is the underlying format for a webpage
  
   Jinga
     Jinga and Jinga II are standalone template engines that a number of Python frmeworks have leveraged instead of writing their own.
   
   Template Engine 
     Django provides it's own template engine that, to ones knowledge, is known simply as the template engine.
     This engine is used only by django and has not been ported to other frameworks
     
   Tornado
     A web framework originating from the twisted project

   Python
     Seriously ?!? A programing language focused upon code readability rather then wearing out the punctuation keys upon ones key board

   :abbr:`PyPI (Python Package Index)` : PyPI
     The Python package index, a.k.a.  The Cheese Shop after the Monty Python Sketch, hosts all of the publicly available Python packages.

   Sphinx
     Pythons' documentation generation utility, often useful in the documentation of other packages in other languages.

   :abbr:`W3C (World Wide Web Consortium)` : W3C
     The world wide web consortium are a body of ICANN, essentially the people that define the internet

   :abbr:`WAI (Web Accessiblity Initiative)` : WAI
     The web accessibility initiative aims to make the web more acccessible to persons with dissabilities
  
