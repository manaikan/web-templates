------
Sphinx
------

.. toctree::
   :maxdepth: 2
   :caption:  Contents:

   Django         <sphinx/django>

`Sphinx: Web Support <https://www.sphinx-doc.org/en/master/usage/advanced/websupport/index.html>`_ provides the machinery to serves ones documentation through their web framework.
`implementation <https://github.com/shimizukawa/sphinx-websupport-app/blob/master/doc/tutorial.rst>`_

.. note::

   `Sphinx: Web Support <https://www.sphinx-doc.org/en/master/usage/advanced/websupport/api.html#sphinxcontrib.websupport.WebSupport>`_ should be preferred over the information that follows.
   
`Sphinx <https://www.sphinx-doc.org/en/master/templating.html>`_ provides three means of incorporating ones documentation into ones web framework.
A fourth method is considered whereby the Jinja2, the HTML builder provided by sphinx, is allowed to execute normally yet the output is decorated such that it may be consumed by a subsequent template engine.

+---------------------------------------+---------------------------------------+---------------------------------------+
| Source processed by Sphinx            | Source processed by second engine     | Final output                          |
+---------------------------------------+---------------------------------------+---------------------------------------+
|::                                     |::                                     |::                                     |
|                                       |                                       |                                       |
|    {% block Jinja2 %}                 |    {% block ENGINE %}                 |     <html>                            |
|    {% block ENGINE %}                 |    <html>                             |      ...                              |
|    <html>                             |     ...                               |     </html>                           |
|     ...                               |    </html>                            |                                       |
|    </html>                            |    {% endblock ENGING %}              |                                       |
|    {% endblock ENGING %}              |                                       |                                       |
|    {% endblock Jinja2 %}              |                                       |                                       |
|                                       |                                       |                                       |
+---------------------------------------+---------------------------------------+---------------------------------------+

The sphinx templates provided by |Project| support this embedding of Sphinx output into some other template engine.
This is down by nesting the syntax of the second engine as comments within the syntax of the first.
This is an experimental feature and may or may not work in all scenarios.

Blocks
======

`Sphinx <https://www.sphinx-doc.org/en/1.5/templating.html#blocks>`_ documents the blocks that it uses within the :file:`layout.html` file provided by its base template.

