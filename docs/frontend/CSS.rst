----------------------
Cascading Style Sheets
----------------------

`How to center in CSS <http://howtocenterincss.com>`_ provides code for doing so. 

There are various means of maintaining `aspect ratio <https://stackoverflow.com/q/1495407>`_.

Display
=======

This attribute accepts variety of values.
`inline-LAYOUT <https://stackoverflow.com/q/24313271/958580>`_

Composites
----------

Consider for example `flex` and `inline-flex`, these would be better named `block-fleck` and `inline-flex`, implying that the element to which this is applied behaves as either a block or an inline element within it's parent and that it's children are all layed out using the flex algorithm.

Hyperlinks
==========

Telephonic Links
----------------

It is probably best to use the ``tel:`` prefix when a number can handle more then one means of communication [#PNA]_ [#PNB]_.

iOS will readily identify and style such elements, this can be disabled by including the following in ones header.
::

    <meta name="format-detection" content="telephone=no">

.. [#PNA] https://css-tricks.com/the-current-state-of-telephone-links/
.. [#PNB] https://www.bounteous.com/insights/2014/01/16/make-phone-numbers-clickable-and-trackable-across-mobile-devices-javascript/

Phone numbers
`````````````

This sets the styling for a phone number, prepending the unicode character for a phone in front of the number.

+----------------------------------------------------------------------------------------------------------------------+
| Cascading Style Sheet                                                                                                |
+----------------------------------------------------------------------------------------------------------------------+
|::                                                                                                                    |
|                                                                                                                      |
|  a[href^="tel:"] { text-decoration: none; }                                                                          |
|  a[href^="tel:"]:before {                                                                                            |
|    content: "\260e";                                                                                                 |
|    margin-right: 0.5em;                                                                                              |
|  }                                                                                                                   |
|                                                                                                                      |
+----------------------------------------------------------------------------------------------------------------------+
| Hyper Text Markup Language                                                                                           |
+----------------------------------------------------------------------------------------------------------------------+
|::                                                                                                                    |
|                                                                                                                      |
|  <a href="tel:000-000-0000" rel="nofollow">000-000-0000</a>                                                          |
|                                                                                                                      |
+----------------------------------------------------------------------------------------------------------------------+

Additionally one can include

``p``
  Provides a 1 second delay before dialing the rest of the number e.g. for navigating a PABx system.
``w``
  Wait for a dial tone before dialing the rest of the number

for handling extentions within a company and/or navigating a call center.

Fax Numbers
```````````

This sets the styling for a fax number, prepending the unicode character for a fax in front of it.

+----------------------------------------------------------------------------------------------------------------------+
| Cascading Style Sheet                                                                                                |
+----------------------------------------------------------------------------------------------------------------------+
|::                                                                                                                    |
|                                                                                                                      |
|  a[href^="fax:"] { text-decoration: none; }                                                                          |
|  a[href^="fax:"]:before {                                                                                            |
|    content: "\260e";                                                                                                 |
|    margin-right: 0.5em;                                                                                              |
|  }                                                                                                                   |
|                                                                                                                      |
+----------------------------------------------------------------------------------------------------------------------+
| Hyper Text Markup Language                                                                                           |
+----------------------------------------------------------------------------------------------------------------------+
|::                                                                                                                    |
|                                                                                                                      |
|  <a href="fax:000-000-0000" rel="nofollow">000-000-0000</a>                                                          |
|                                                                                                                      |
+----------------------------------------------------------------------------------------------------------------------+

SMS Numbers
```````````

This sets the styling for a fax number, prepending the unicode character for a fax in front of it.

+----------------------------------------------------------------------------------------------------------------------+
| Cascading Style Sheet                                                                                                |
+----------------------------------------------------------------------------------------------------------------------+
|::                                                                                                                    |
|                                                                                                                      |
|  a[href^="sms:"] { text-decoration: none; }                                                                          |
|  a[href^="sms:"]:before {                                                                                            |
|    content: "\260e";                                                                                                 |
|    margin-right: 0.5em;                                                                                              |
|  }                                                                                                                   |
|                                                                                                                      |
+----------------------------------------------------------------------------------------------------------------------+
| Hyper Text Markup Language                                                                                           |
+----------------------------------------------------------------------------------------------------------------------+
|::                                                                                                                    |
|                                                                                                                      |
|  <a href="sms:000-000-0000" rel="nofollow">000-000-0000</a>                                                          |
|                                                                                                                      |
+----------------------------------------------------------------------------------------------------------------------+

Images
======

One should set both the `Smashing Magazine: Set Height and Width <https://www.smashingmagazine.com/2020/03/setting-height-width-images-important-again/>`_ and use these values to assign a consistent `Mozilla: Aspect Ration <https://developer.mozilla.org/en-US/docs/Web/CSS/aspect-ratio>`_. ::

+----------------------------------------------------------------------------------------------------------------------+
| Cascading Style Sheet                                                                                                |
+----------------------------------------------------------------------------------------------------------------------+
|::                                                                                                                    |
|                                                                                                                      |
|    img {                                                                                                             |
|        width: 100%;                                                                                                  |
|        height: auto;                                                                                                 |
|        aspect-ratio: attr(width) / attr(height);                                                                     |
|    }                                                                                                                 |
|                                                                                                                      |
+----------------------------------------------------------------------------------------------------------------------+
| Hyper Text Markup Language                                                                                           |
+----------------------------------------------------------------------------------------------------------------------+
|::                                                                                                                    |
|                                                                                                                      |
|    <img width="..." height="..." ...></img>                                                                          |
|                                                                                                                      |
+----------------------------------------------------------------------------------------------------------------------+

.. note::

    One might employ a similar technique for Charts.