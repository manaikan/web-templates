.. Templates documentation master file, created by
   sphinx-quickstart on Tue Jun 12 17:35:04 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

-------------
Web Templates
-------------

.. toctree::
   :maxdepth: 1
   :caption:  Contents:

   Setup         <setup>
   Django        <django>
   Flask         <flask>
   Quart         <quart>
   Sphinx        <sphinx>
   Tornado       <tornado>
   Template(s)   <templates>
   Extention     <extentions>
   Design        <design>
   FrontEnd      <frontend>
   Contribution  <contribution>
   Glossary      <glossary>

.. rubric:: Problem

There isn't a standardized *base* template that the authors(s) of websites and the author(s) of extentions for a given web framework can collectively reference.
The website author specifies the *base* template for their website per their requirements, populating it with the components provided by framework extentions.
The authors of framework extentions can not know the final structure of the *base* template for a given website and must assume which block such templates might provide.

.. mermaid::
   :align: center

   graph RL
       A[extention/base.html] -->|?| B[base.html]

.. The extention package(s) of a given web framework can not assume the existence of a standardized *base* template, that the authors(s) of a website, using such packages should implement.
.. If such a template inherited a **skeleton** template common to that framework then its extention packages could appear as the natural extention of the web author(s) site.
.. It might even be possible to make the respective templates in such packages agnostic of the template engine used to render them.

.. rubric:: Objective

The package is to provide a standard structure/methodology for both the website and extention author(s) of the respective frameworks.

.. This package hopes to provide such `skeleton` templates for each framework and its extention packages.
.. Author(s) using these frameworks, and their extention packages, then implement a *base* template that extends the common **skeleton** template provided by this package.

.. This could save the author from having to reimplement a *base* template for each project.
.. It prevents the eventuality that the *base* template from one extention differs incompatably from that of another; leaving it to the author relying on both extentions to resolve.

.. rubric:: Scope

|Project| focuses primarily upon the Python web frameworks relying upon templating mechanism

.. rubric:: Proposal

A **skeleton** template that both the authors of websites and of framework extentions can reference for their chosen framework.
This has the following benefits :

 * The webiste author(s) using these frameworks, and their extention packages, can customize their *base* template, that extends the **skeleton** template, against a standardized set of guidelines.
 * The extention author(s) of these frameworks can, reasonably, presume the existance of a standardized *base* template.

.. mermaid::
   :align: center

   graph RL
       A[extention/base.html] --> B[base.html]
       B --> C[skeleton.html]
     A --> C

.. rubric:: Solution

|Project| provides a **skeleton** with the minimal structure necessary to produce a standardized *scaffold* for the `HTML <https://html.spec.whatwg.org/>`_  documents for use with Python's respective Web Frameworks (Django/Tornado/Flask/etc.).
The "ideal" **skeleton** should produce the following *scaffold* after being passed through ones template engine [#template]_ :

.. table:: Ideal **skeleton** and resulting *scaffold*.
    :widths: auto
    :align: center

    +-----------------------------+-----------------------------+
    | Skeleton                    | Scaffold                    |
    +=============================+=============================+
    |  ::                         |  ::                         |
    |                             |                             |
    |      {% block html dtd %}   |      <!DOCTYPE html>        |
    |      <!DOCTYPE html>        |      <html>                 |
    |      {% endblock dtd %}     |          <head></head>      |
    |      {% block html %}       |          <body></body>      |
    |      <html>                 |      </html>                |
    |        {% block head %}     |                             |
    |        <head>               |                             |
    |        </head>              |                             |
    |        {% endblock head %}  |                             |
    |        {% block body %}     |                             |
    |        <body>               |                             |
    |        </body>              |                             |
    |        {% endblock body %}  |                             |
    |      </html>                |                             |
    |      {% endblock html %}    |                             |
    |                             |                             |
    +-----------------------------+-----------------------------+

.. rubric:: Organization

The documentation is partitioned into sections that are pertinent to the package users or website authors, third parties providing framework extentions and package contributors.

:ref:`Setup <setup:Setup>` describes the :ref:`installation <setup:Installation>` and :ref:`removal <setup:Uninstallation>` of |Project|.
Then following section(s), each dedicated respectively to a framework, details the configuration and usage of |Project| within ones project(s).
Each section prescribes a standardized *base* template, where possible, and details the underlying **skeleton** that it extends.

Third parties incorporating |Project| into their projects should read the :ref:`extentions <extentions:Extentions>` section along with the section relevant to the framework(s) they support.
The rational, nomenclature and structure of the **skeleton** is described in :ref:`Template <templates:Template(s)>`.

Finally the package :ref:`design <design:Design>` is described and how one might :ref:`constribute <contribution:Contribution>`.

.. The project should eventually result in a canonical set of templates generic enough any project.
.. Originally intended for django projects this has come to be extended for other projects too.
.. Wierdly this package contains no Python code, whatsoever, providing only the templates necessary for other packages, frameworks and projects.

.. note ::

   This package only supports Python 3.4 and onwards as it relies upon name-spaced packages (See :pep:`420` and :ref:`contribution/packages:Name Spaced Packages`).

.. rubric:: footnotes

.. [#template] The package actually extends this "ideal" **skeleton** to provide a :ref:`template <templates:template>` with a more practical alternative.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
