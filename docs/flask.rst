-----
Flask
-----

Web templates can be used with a flask application as follows ::

    from flask import Flask
    from webplates.flask.templates import register as register_webplates

    app = Flask()
    app = register_webplates(app)

Alternatively one may simply copy the :file:`skeleton.html` file into ones projects' template directory.

Mixin
=====

|Project| also provides s mixin class for rendering templates ::

    from webplates.flask.templates import TemplateMixin

    class VIEW(TemplateMixin, ...):
       ...
