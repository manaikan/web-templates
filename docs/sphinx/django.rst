------
Django
------

|Project| provides :attr:`sphinx_django` as the compliment to Sphinxs' basic theme.
To build ones documentation such that it may be embedded into Django it is most convenient to override the ``html_theme`` setting at compilation time.

.. code-block:: console

    sphinx-build -D html_theme=sphinx_django [OPTIONS] SOURCE TARGET [FILES...]

Then follow the instructions under :ref:`Django-Sphinx <django/sphinx:Sphinx>`.

Templates
=========

The following have been observed within the Sphinx templates :

 * Semantic tags are not used within their basic template for some reason; instead they rely upon ``div.document`` and related classes.
 * ``sidebar1`` is structurally alongside ``div.document`` div while ``sidebar2`` is nested within it.
 * Semantic tags are not used within their basic template for some reason; instead they rely upon ``div.document`` and related classes.

Skeleton
========

This is a complete replacement for the Sphinx Basic theme and relies heavily upon the work around described within :ref:`templates:Init and Term TAGS`.

The relbar element and the footer div setup by sphinx are both bundled together for the moment and clobber the ``footer`` tag from the layout.
Ideally the footer div in the `footer` block of Sphinx should substitute the ``footer_content`` in the Django skeleton.
Similarly ``relbar2`` would best be placed into the ``aside`` element.

Basic
=====

This tries to include the Sphinx templates and includes this as a dependency.
This is largely done as an example so that others might extend it for the other Sphinx themes.

