-------------
Documentation
-------------

Sphinx is the de facto documentation tool used within Python.
Sphinx can readily be used to document the various Python frameworks and in some cases the frameworks will allow one to embed the resulting documentation into ones website.

Sphinx - Django
===============

To document a Django project one need only include the following lines within their configuration file::

.. code-block::
    :caption: :file:`DOCS/conf.py`

    import os
    import sys
    import django

    sys.path.insert(0, os.path.abspath('..'))
    os.environ['DJANGO_SETTINGS_MODULE'] = 'website.settings'
    django.setup()

Django - Sphinx
===============

One can embed their Sphinx output into their website using the Django: Sphinxdoc package; this depends upon both haystack and whoosh.
Necessarily `sphinxdoc` and `haystack` must be included in the list of ``INSTALLED_APPLICATIONS`` within ones Django configuration. ::

    INSTALLED_APPLICATIONS += ['sphinxdoc','haystack']

Sphinxdoc must be configured to use the sphinxdoc template ::

    SPHINXDOC_BUILD_DIR = str( BASE_DIR / 'docs' / '.build' ) # The source Folder of ones documentation
    SPHINXDOC_BASE_TEMPLATE = "sphinxdoc.html"                # A Web-Template provided by this package

.. todo::

    The Gander and Quantopia Projects are waiting for this template to be made available in the next version bump of Web-Templates.

and haystack requires a search backend to be configured, of which `whoosh` is the easiest ::

    HAYSTACK_CONNECTIONS = {
        'default': {
            'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
            'PATH': BASE_DIR / 'build' / 'whoosh',
        },
    }

Additionally a record under ``Project`` is required under ``Sphinxdoc`` within the database; to build the documentation.

Finaly include the following entry within ones URL patterns ::

    url_patterns += [
        path('docs/',     include('sphinxdoc.urls'), name='docs'),
    ]

Quirks
------
.. date:: 2022/04/20

Sphinxdocs has a number of Quirks that must be resolved and for this reason it is best to have a virtual environment within which to work.

 * `SO: Attr Error <https://stackoverflow.com/a/63377228>`_ is resolved by simply editing the source for Sphinxdoc.

 *  When one successfully builds the documentation and opens it within Django, Sphinxdoc might return with a 22 error.
    Sphinxdoc returns this error when it cannot find the :file:`last_build` folder; either create it under :file:`docs/.builds/json/` or rebuild the documentation.
    Under the hood Sphinxdoc does not honour the `SPHINXDOC_BUILDDIR` setting and can't find the output from a build as a result.
