-----
Usage
-----

Blocks are named to distinguish the hierarchy of the templates (site, page, view), javascript (`<script/>`), style (`<style/>`), meta (`<dtd/>` and `<meta/>`) and structural elements (Structural elements, `<html>`,`<head>`,`<body>` and layout elements, `<header/>`, `<main/>` `<footer/>` and `<aside/>`).

.. production:: Web Templates
    block :: `site` | `html` | `meta` | `js` | `css` | `style`

Hierarchy
~~~~~~~~~

Templates organize blocks according to generality withina webaite.
The `site` wise setup applies to the :file:`template/base.html` template.
Web applications are to populate the `page`, or `application`, portion of a template within their :file:`template/PAGE/base.html`, :file:`template/APPLICATION/base.html`.
Finally `view` is reserved for the final template that ultimately is rendered by view :file:`template/APPLICATION/base.html`

.. production:: Web Templates
    site :: 'site' | 'page' | 'view'

.. note::

    Single page applications will tend to overwrite only the `site` or `page` sections

HTML tags
~~~~~~~~~

The HTML tag blocks are structured as follows.

.. production:: Web Templates
    html :: `tag` + '_' + `injector` | `terminator`

The templates deal only with the following elements.

.. production:: Web Templates
    tag :: `structural` | `sectional`
    structural :: 'head' | 'body' | 'foot'
    sectional :: 'header' | 'main' | 'aside' | 'footer'

Metadata
~~~~~~~~

Metadata tags use the following structure

.. production:: Web Templates
    meta :: `dtd` | ( `meta` + '_' `injection` )

Javascript
~~~~~~~~~~

Metadata tags use the following structure

.. production:: Web Templates
    javascript :: `js` + [ '_' + `injection` ]

Style Sheets
~~~~~~~~~~~~
.. production:: Web Templates
    style ::  `css` + [ '_' + `injection` ]

Modifiers
~~~~~~~~~

Termination
^^^^^^^^^^^

This might be used to wrap a tag as in the Django templates as one cannot overwrite the tag attributes otherwise.

.. production:: Web Templates
    terminator :: 'open'|'close'

Injection
^^^^^^^^^

These are used to prepend or append content.

.. production:: Web Templates
    injection :: 'prefix'|'suffix'
