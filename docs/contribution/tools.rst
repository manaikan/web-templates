-----
Tools
-----

This section reviews some of the tools available to the web developer.

HTML Validation
===============

HTML validation can be done through the `W3.org validator <http://validator.w3.org/>`_ .

Symbolic Links
--------------

Symbolic links may be made on most systems these days. 
To do so on windows use the following ::

  mklink /J TARGET SOURCE

While linux users should  employ
:: 

  ln -s source target

.. note ::

   Originally the templates where made available to the projects one was sorking on by symbolic links.
   This made the same files available to multiple projects with different requirements and proved to be very inflexible and prompted the proper packaging of these templates.

Crimson Editor
==============

The Jinja template is generated from the Django template by copying it and performing the following substitutions.
::

      {%([^%]*)%}     ->      {{'{%\1%}'}}   
  regular expression      replacement pattern

.. Note :: The regular expression and assosciated pattern are specific to crimson editor.

