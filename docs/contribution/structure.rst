---------
Structure
---------

Package Structure
=================

The package structure is outlined below and serves as a guideline for those providing competing and/or complementary features.
::

  Project/                         # Project/Repsoitory root
   templates/                      # Namespace reserved for web-templates
    django/                        # Namespace reserved for Django templates
      templates/skeleton.html      # Django skeleton template
    flask/                         # Namespace reserved for Flask templates
    tornado/                       # Namespace reserved for Tornado templates
    sphinx/                        # Namespace reserved for sphinx templates
    django.html                    # Reserved name for a Django template engine template
    jinja.html                     # Reserved name for a Jinja/Jinja2 template
   setup.py                        # Setup.py script that bundles everything together
   MANIFEST.in                     # Manifest file that embeds the various data sources into the distributed package.

This package reserves the package namespace :mod:`templates.*` by providing a namespaced package/folder with the the name :file:`templates` in the project root.
Furthermore the template names :file:`django.html` and :file:`jinja.html` are reserved for the base HTML templates used by the Django and Jinja templating systems respectively.

.. note ::

   Given the package/template path :file:`package/django/templates/skeleton.html` one includes :mod:`package.django` within :attr:`INSTALLED_APPLICATIONS` and extends :file:`skeleton.html` within their :file:`base.html` template.
   Django knows to look for the templates under the :file:`templates` subfolder.
   

Structure limitations
=====================

The desired structure for the core of the package is as follows 
::

  project\
   templates\
    django\templates\skeleton.html # Combines base.structure.html and base.html into a single file, currently available as 
    sphinx\templates\skeleton.html # Combines `base.sphinx.html`, `basic.layout.html` and `basic.layout - copy.html` into a single file
    jinja\templates\skeleton.html  # Combines `base.jinja.html` into a single file
    
Previously the following alternatives have been attempted but they fail for various reasons.

   +------------------------------------------------+------------------------------------------------+
   | ::                                             | ::                                             |
   |                                                |                                                |
   |   templates\                                   |   templates\                                   |
   |    base.html                                   |    templates\base.html                         |
   |   __init__.py                                  |    __init__.py                                 |
   |                                                |                                                |
   +------------------------------------------------+------------------------------------------------+
   | Unpackageable                                  | Makes :mod:`template` into an explicit package |
   +------------------------------------------------+------------------------------------------------+

Django expects that a :file:`templates` folder exists containing the templates.
In the first structure Django simply ignores the :file:`base.html` as a result.
In the second case Django finds the file as appropriate but it does not like the fact that the root package is namespaced.
The work around is to nest the package one level deeper to appease Django and get the user to list :mod:`templates.django` under their `INSTALLED_APPS` in their :file:`settings.py` file.

Artefacts
=========

The following templates are the leftovers from prior development attempts.
::

  basic/              Additional Sphinx related templates (Not sure on status or usage)
   layout.html        This embeds the jinja2 tags expected by sphinx within the django template tags (not sure how this differs from base.sphinx.html).
  base.django.html    The django base template as prescribed by modern HTML5; Originally base.structure.html
  base.sphinx.html    This is adapted from base.structure.html to accomodate Sphinx projects.
  base.jinja.html     This is adapted from base.structure to accomodate jinja as the template engine.
  base.html           This is adapted from base.structure to accomodate JavaScript libraries, this needs more work i.e. selectively enabling one or another library.

For the most part these are to be deprecated/deleted but there is still some value to extracted before doing so.
The templates under the :file:`basic` should really be merged/compared against the :file:`base.sphinx,html` file for instance as this has allowed me to embed sphinx documentation within a Django site before for instance.
