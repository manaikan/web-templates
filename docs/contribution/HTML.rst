------
|HTML|
------

The |W3C| defines the specification for HTML, CSS and Javascript (a.k.a. ECMA script).
This section tries to summarize/link to the relevant information that they provide.

|W3C|
=====

The |W3C| has setup the `Web accessiblity Initiative <www.w3.org/WAI/>`_ which aims to make the web more accessible to everyone.
The project should follow these recommendations as best as possible.

Schema
======

The `Schema <https://schema.org/>`_ or `Data Vocabulary <http://www.data-vocabulary.org/>`_ provides assistive information to the search engine.

|DTD|
=====

The W3 Schools lists the `HTML tags <https://www.w3schools.com/tags/ref_html_dtd.asp>`_ supported by each DTD type.

