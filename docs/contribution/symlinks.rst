Templates
=========

Django used to search the :file:`PROJECT/WEBSITE/templates` folder for common templates, it no longer does so and expects a :file:`templates` folder to reside in the project root, :file:`PROJECT/templates`.
Should one opt to their templates within the website directory, under :file:`PROJECT/WEBSITE/templates`, then they should include them within their project settings by assigning the :attr:`templates[dirs]` attribute in the :file:`WEBSITE/settings.py` file.
::

    TEMPLATES = {...
     DIRS = ['website/templates'],
    ...}

.. One also needs to inform django where they placed their templates should they not be linked from an application directory but rather from the website directory.
