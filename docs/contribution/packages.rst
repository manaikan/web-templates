--------
Packages
--------

Name Spaced Packages
====================

Classic/Conventional Python packages consist of a folder containing a, typically empty, :file:`__init__.py` file to identify them as such.
Python 3.4 introduced namespaced packages which consist of a folder containing further sub-packages or :file:`MODULE.py` file(s), none of which are named :file:`__init__.py`.
Conveniently :mod:`setuptools` has allowed package writers to embed "data" files within their packages for sometime.
Combining these properties together affords one the ability to distribute Python packages that, ironically, contain no Python code.

Furthermore namespaced packages act as mixins.
That us any two modules nested under a common package path, neither of which may contain an :file:`__init__.py` file, may be imported simultaneously as though they were part of the same package.

.. topic:: Example :

   As an example consider the two files, :file:`MODULE_A.py` and :file:`MODULE_A.py`; both of which are nested under a folder with the same name, :file:`PACKAGE`, but located at different places upon ones file system.
   ::
   
     this/project     # Some path in sys.paths
      PACKAGE/        # Common package path
       ...            # Other modules none of which are named __init__.py
       MODULE_A.py    # Module in the namespace'd set
     that/project     # Some path in sys.paths
      PACKAGE/        # Common package path
       ...            # Other modules none of which are named __init__.py
       MODULE_B.py    # Module in the namespace'd set
   
   Python views both files as though they were modules, :mod:`MODULE_A` and :mod:`MODULE_B`, under the same namespaced package, :mod:`PACKAGE`, making it possible to perform the following imports
   ::
   
     from PACKAGE import MODULE_A
     from PACKAGE import MODULE_B
     
   This is not possible with classical Python packages, in fact adding an :file:`__init__.py` file to either package breaks the above import statements.
   
Effectively one may create a namespaced package that contains only `data` and will merge well with other packages that do the same.
Under the mild restriction that such packages do not include an :file:`__init__.py` file within their structure.



