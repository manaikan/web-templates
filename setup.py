#!/usr/bin/env python
"""
Develop
-------

To setup a development installation one may call either pip as 
follows;
::
   pip install -e .
 
or execute the setup script directly;
::
   python setup.py develop

The pip version is generally preferred.

Distribution
------------

`Hynek Schlawack <https://hynek.me/talks/python-foss/>`_ on Python packaging

Clear out any old builds from the dist directory, PyPI won't let one upload a new package along with older ones.
One is probably meant to preserve these releases and only ship the "latest copy" come to think of it. ::

    rmdir /S /Q dist 

Check the package prior to packageing it (see also readme-renderer and collective.checkdocs) ::

    python setup.py check -r -s
    python setup.py check --restructuredtext

Build the pacakge as wheel for distribution ::

    python setup.py bdist_wheel

Check that the package is properly setup ::

    twine check dist/*
     
Distribute the package to the test server; I know, I know ! just do it ! ::

    twine upload --repository-url https://test.pypi.org/legacy/ dist/*

Distribute the package to the PyPI server ::

    twine upload dist/*

Documentation
-------------

Documentation may be generated using the setup script.
::
   python setup.py build_sphinx -b singlehtml|html|latex

Select between singleHTML, HTMl and LaTeX as necessary.
The output is saved under :file:`dist/singlehtml|html|latex`.
"""
import os
from setuptools import setup, find_packages
# from distutils.core import setup
from webplates import __meta__

with open("README.rst", "r") as file:
    description = file.read()

setup(
 name                 = __meta__.__project__,
 version              = __meta__.__release__, # Python/PyPi takes the release number as the version number
 description          = __meta__.__summary__,
# long_description     = open(os.path.join(os.path.dirname(__file__), 'README.rst')).read(),
 url                  = __meta__.__website__,
 author               = __meta__.__author__,
 author_email         = __meta__.__e_mail__,
 long_description     = description,
 long_description_content_type = "text/x-rst",
#  license              = 'PSFL',
#  platforms            = 'any',
#  install_requires     = ['six',
#                          'future',
#                          'pathlib;python_version=="2.7"',
#                          'mock;python_version=="2.7"'], # See PEP 508 : Environmental Markers
#  scripts            = [], # Command Line callable scripts e.g. '/Path/To/Script.py'
 ## Provide the Package
# packages             = find_packages(exclude=["docs","tests",".git"]),
 packages             = ["webplates"],
 ## Provide the Package and the templates as package_data
#  packages             = ['templates'],
#  package_dir          = {"templates":"templates"},
#  package_data         = {"templates":["*.html"]}, # https://docs.python.org/2/distutils/setupscript.html#installing-additional-files
 ## Provide the templates as data_files
#  data_files           = [("templates",["templates/base.html"])], # https://docs.python.org/2/distutils/setupscript.html#installing-additional-files
 ## Provide the templates as via the manifest
 include_package_data = True,
 zip_safe             = False,
#  test_suite           = "tests",
#  tests_require        = [# 'tox',                             # Uncomment to enable ToX
#                          'apeman-overlays',
#                          # 'unittest2;python_version=="2.7"', # Six offers a shim that negates the need for unittest2
#                         ], 
# #  cmdclass             = {'test': Tox}                       # Uncomment to enable ToX
#  entry_points = {
#   'sphinx_themes': [
#          'path = webplates:sphinx_themes', # This must return a list of directories where sphinx themes can be found
#      ]
#  },
 # The following is all that is required to register the Manaikan theme
 # entry_points = { "sphinx.html_themes" : ["Templates = templates.sphinx",]},
 entry_points = { "sphinx.html_themes" : [
  "sphinx_django_skeleton = webplates.sphinx.django.skeleton",
  "sphinx_django_base = webplates.sphinx.django.base",
  # "Manaikin = webplates.sphinx.manaikan",
  # "Oval_Africa = webplates.sphinx.oval_africa",
 ]},
 command_options      = {'build_sphinx': {
                          'version'    : ('setup.py', __meta__.__version__),
                          'release'    : ('setup.py', __meta__.__release__),
                          'source_dir' : ('setup.py','docs'),
                          'build_dir'  : ('setup.py','build'),
                          'config_dir' : ('setup.py','docs'),}}, 
 classifiers          = [ # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
  "Development Status :: 3 - Alpha",
  "Intended Audience :: Developers",
  "Framework :: Flask",  # Associates the package with the Flask Framework
  "Framework :: Django", # Associates the package with the Django Framework
#   "Framework :: Apeman :: Overlay ",
#   "License :: OSI Approved :: Python Software Foundation License",
#   "Natural Language :: English",
  "Operating System :: OS Independent",
  "Programming Language :: Python",
#   "Programming Language :: Python :: 2",
#   "Programming Language :: Python :: 2.7",
#   "Programming Language :: Python :: 3",
#   "Programming Language :: Python :: 3.4",
#   "Programming Language :: Python :: 3.5",
#   "Programming Language :: Python :: 3.6",
#   "Programming Language :: Python :: 3.7",
#   "Programming Language :: Python :: 3.8",
#   "Topic :: Software Development :: Libraries :: Python Modules",
  "Topic :: Utilities",
 ],
)
